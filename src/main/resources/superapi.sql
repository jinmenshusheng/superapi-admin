/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : superapi

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2017-12-27 17:58:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL COMMENT '项目id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '所属模块id',
  `name` varchar(255) NOT NULL COMMENT '接口名称',
  `url` varchar(255) DEFAULT NULL COMMENT '接口路径',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `req_type` varchar(255) DEFAULT NULL COMMENT '请求类型',
  `req_param` varchar(2000) DEFAULT NULL COMMENT '请求参数',
  `req_header` varchar(500) DEFAULT NULL COMMENT '请求头',
  `resp_data` varchar(2000) DEFAULT NULL COMMENT '响应数据',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `mock_value` varchar(1000) DEFAULT NULL COMMENT 'mock值',
  `mock_rule` varchar(255) DEFAULT NULL COMMENT 'mock表达式',
  `discard` int(255) DEFAULT NULL COMMENT '是否废弃',
  `update_date` datetime DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of api
-- ----------------------------
INSERT INTO `api` VALUES ('1', '16', '0', '用户管理模块', 'a/a', 'aa', null, 'get', null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('2', '16', '1', '登录', 'user/login', '第1步：验证手机号是否已注册，如果未注册，则结束，返回“该手机号未注册”；否则至第2步；\n第2步：根据手机号查询用户当前登录错误次数，如果该次数大于等于数据字典中配置的需要验证码的密码错误次数，则验证码不能为空。如果为空，返回“验证码不能为空”，如果验证码错误，则返回“验证码错误”\n第3步：手机号、密码是否匹配，不匹配则更新密码错误次数，返回“密码错误”；如果验证一致，跳转第4步；\n第4步：如果登录来源为安卓或IOS，验证此次登录设备号是否与上次最后登录设备号一致，如果不一致，则登录结束，跳转到设备更新页面；如果一致，则到第5步；如果登录来源不为安卓或IOS，则跳转到第5步；\n第5步：登录成功后，更新用户表中最后登录信息并插入登录日志，返回“登录成功”。', null, 'post', '[{\"tid\":0,\"name\":\"phone\",\"type\":\"string\",\"isNeed\":true,\"description\":\"手机号\",\"mockValue\":\"15921977515\"},{\"tid\":1,\"name\":\"password\",\"type\":\"string\",\"isNeed\":true,\"description\":\"密码\"},{\"tid\":2,\"name\":\"clientid\",\"type\":\"string\",\"isNeed\":true,\"description\":\"推送客户端id\"},{\"tid\":3,\"name\":\"login_ip\",\"type\":\"string\",\"isNeed\":true,\"description\":\"登录地IP\"},{\"tid\":4,\"name\":\"login_platform\",\"type\":\"string\",\"isNeed\":true,\"description\":\"登录来源\",\"remark\":\"JJT_1：急借通Android\\nJJT_2：急借通IOS\\nJJT_3：急借通h5\\nYXT_1：盈信通Android\\nYXT_2：盈信通IOS\\nYXT_3：盈信通h5\\n9：未知来源\"},{\"tid\":5,\"name\":\"login_device_no\",\"type\":\"string\",\"isNeed\":false,\"description\":\"\",\"remark\":\"登录设备号(设备版本号)\"},{\"tid\":6,\"name\":\"longitude\",\"type\":\"string\",\"isNeed\":false,\"description\":\"登录经度\"},{\"tid\":7,\"name\":\"latitude\",\"type\":\"string\",\"isNeed\":false,\"description\":\"登录纬度\"},{\"tid\":8,\"name\":\"phone_equipment\",\"type\":\"string\",\"isNeed\":true,\"description\":\"手机设备SDK标识\"}]', '[]', null, 'admin', null, null, null, null);
INSERT INTO `api` VALUES ('3', '16', '1', '注册', 'a/aa', '第1步：验证手机号是否已注册，如果已注册，则结束，返回“该手机号已注册”；否则至第2步；\n第2步：判断验证码是否正确，不正确则结束，返回“验证码不正确”；否则至第3步； \n第3步：如果注册来源为“Android”或“Ios”，则注册设备号不能为空。如果为空，则返回“注册来源为xxx,注册设备号不能为空”，否则跳转到第4步；\n第4步：新增用户信息到用户表，返回“注册成功”。\n', null, 'post', '[{\"tid\":0,\"name\":\"phone\",\"type\":\"string\",\"isNeed\":true,\"description\":\"手机号\"},{\"tid\":1,\"name\":\"clientid\",\"type\":\"string\",\"isNeed\":false,\"description\":\"客户端id\"},{\"tid\":2,\"name\":\"verify_code\",\"type\":\"int\",\"isNeed\":true,\"description\":\"验证码\"},{\"tid\":3,\"name\":\"password\",\"type\":\"string\",\"isNeed\":true,\"description\":\"密码\"},{\"tid\":4,\"name\":\"recommend_code\",\"type\":\"string\",\"isNeed\":false,\"description\":\"推荐码\"}]', '[]', null, 'admin', null, null, null, null);
INSERT INTO `api` VALUES ('4', '16', '0', '项目管理模块', 'a/a', 'bb', null, 'get', null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('5', '16', '4', '所有项目', 'project/list', '查询所有项目', null, 'get', '[]', '[{\"tid\":0,\"key_\":\"token\",\"value_\":\"e906a66d1a86f516029ec6fb6b201cd6\",\"description\":\"\"}]', null, 'admin', null, null, null, null);
INSERT INTO `api` VALUES ('6', '16', '4', '新增项目', 'project/put', '新建项目', null, 'post', '[{\"tid\":0,\"name\":\"userIds\",\"type\":\"string\",\"isNeed\":true,\"description\":\"用户id字符串\",\"remark\":\"例：1,2,3\"},{\"tid\":1,\"name\":\"name\",\"type\":\"string\",\"isNeed\":true,\"description\":\"项目名称\"},{\"tid\":2,\"name\":\"description\",\"type\":\"string\",\"isNeed\":false,\"description\":\"项目描述\"}]', '[]', null, 'admin', null, null, null, null);
INSERT INTO `api` VALUES ('7', '16', '4', '删除项目', 'a/a', 'ee', null, 'get', null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('8', '16', '4', '修改项目', 'a/a', 'ff', null, 'get', null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('9', '16', '0', '接口管理模块', 'a/a', 'ff', null, 'get', null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('10', '16', '9', '新增接口', 'a/a', 'ff', null, 'get', null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('11', '16', '9', '删除接口', 'a/a', 'ff', null, 'get', null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('12', '16', '0', '日志管理', 'a', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('19', '16', '0', 'aaa', 'a', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('28', '16', '19', 'bbb', 'a', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('31', '16', '1', '查询所有人员', 'user/list', '查询所有人员', null, 'get', '[]', '[]', null, 'admin', null, null, null, null);
INSERT INTO `api` VALUES ('32', '15', '0', '用户模块', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('33', '15', '32', '登录', null, 'aaa', null, null, '[]', '[]', null, null, null, null, null, null);
INSERT INTO `api` VALUES ('34', '15', '0', '客户进件', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('35', '15', '34', '客户基本信息获取', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('36', '15', '34', '客户基本信息保存', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('37', '15', '34', '客户认证信息保存', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('38', '15', '34', '客户认证信息获取', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('39', '15', '34', '客户职业信息保存', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('42', '15', '32', '注册', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('43', '15', '32', '忘记密码', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('44', '15', '32', '发送注册验证码', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('49', '15', '0', '客户提款', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('50', '15', '49', '审批金额查询', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('51', '15', '0', '还款', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('52', '15', '51', '应还金额查询', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('53', '15', '51', '还款', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('54', '15', '51', '一次结清', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('55', '15', '49', '申请件状态查询', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('56', '15', '34', '客户职业信息获取', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('57', '20', '0', '用户模块', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('63', '20', '0', '接口模块', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('64', '20', '0', '项目模块', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `api` VALUES ('65', '20', '64', '查询我加入的项目', 'project/list', '查询我加入的项目，非开发人员无法编辑，非项目经理无法新建项目', null, 'get', '[]', '[{\"tid\":0,\"key_\":\"token\",\"value_\":\"400c247bdd123a6391b350c65ea3738c\",\"description\":\"\"}]', null, 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('67', '20', '64', '新增或修改项目', 'project/put', '新增或修改项目名称，描述，成员', null, 'post', '[{\"tid\":0,\"name\":\"id\",\"type\":\"int\",\"isNeed\":true,\"description\":\"项目id\"},{\"tid\":1,\"name\":\"name\",\"type\":\"string\",\"isNeed\":true,\"description\":\"项目名称\"},{\"tid\":2,\"name\":\"description\",\"type\":\"string\",\"isNeed\":false,\"description\":\"项目描述\"},{\"tid\":3,\"name\":\"userIds\",\"type\":\"string\",\"isNeed\":false,\"description\":\"成员id字符串\",\"remark\":\"例：1,2,3\"}]', '[]', '[]', 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('68', '20', '63', '添加模块', 'api/put', '添加模块', null, 'post', '[{\"tid\":0,\"name\":\"name\",\"type\":\"string\",\"isNeed\":true,\"description\":\"模块名称\"}]', '[]', '[]', 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('69', '20', '63', '修改接口', 'api/put', '修改接口', null, 'post', '[]', '[]', '[]', 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('70', '20', '57', '登录', 'user/login', 'superAPI系统登录', null, 'post', '[{\"tid\":0,\"name\":\"name\",\"type\":\"string\",\"isNeed\":true,\"description\":\"用户姓名拼音\",\"mockValue\":\"admin\"},{\"tid\":1,\"name\":\"password\",\"type\":\"string\",\"isNeed\":true,\"description\":\"用户密码\",\"mockValue\":\"admin123\"}]', '[]', '[{\"tid\":0,\"name\":\"id\",\"description\":\"用户id\",\"remark\":\"\"},{\"tid\":1,\"name\":\"name\",\"description\":\"用户名称\",\"remark\":\"\"},{\"tid\":2,\"name\":\"pm\",\"description\":\"是否是项目经理\",\"remark\":\"\"},{\"tid\":3,\"name\":\"token\",\"description\":\"登录token\",\"remark\":\"\"}]', 'tianyu', null, null, null, '2017-12-27 13:58:47');
INSERT INTO `api` VALUES ('71', '20', '57', '注册', 'user/regist', '注册用户，如果用户名已注册，提示用户', null, 'post', '[{\"tid\":0,\"name\":\"name\",\"type\":\"string\",\"isNeed\":true,\"description\":\"用户名\"},{\"tid\":1,\"name\":\"password\",\"type\":\"string\",\"isNeed\":true,\"description\":\"密码\"},{\"tid\":2,\"name\":\"positioinId\",\"type\":\"int\",\"isNeed\":true,\"description\":\"职位id\"}]', '[]', '[{\"tid\":0,\"name\":\"token\",\"description\":\"登录标识\",\"remark\":\"\"},{\"tid\":1,\"name\":\"name\",\"description\":\"用户名\",\"remark\":\"\"},{\"tid\":2,\"name\":\"pm\",\"description\":\"是否是项目经理\",\"remark\":\"\"},{\"tid\":3,\"name\":\"id\",\"description\":\"用户id\",\"remark\":\"\"}]', 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('72', '20', '57', '查询所有成员', 'user/list', '查询所有成员姓名', null, 'get', '[{\"tid\":0,\"name\":\"id\",\"type\":\"int\",\"isNeed\":true,\"description\":\"\",\"mockValue\":\"aaa\"}]', '[]', '[{\"tid\":0,\"name\":\"id\",\"description\":\"id\",\"remark\":\"\"},{\"tid\":1,\"name\":\"name\",\"description\":\"用户名\",\"remark\":\"\"}]', 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('73', '20', '64', '查询项目详细信息', 'project/get', '查询项目信息', null, 'get', '[{\"tid\":0,\"name\":\"id\",\"type\":\"int\",\"isNeed\":true,\"description\":\"项目id\",\"mockValue\":\"15\"}]', '[]', '[{\"tid\":0,\"name\":\"name\",\"description\":\"项目名\",\"remark\":\"\"},{\"tid\":1,\"name\":\"description\",\"description\":\"描述\",\"remark\":\"\"},{\"tid\":2,\"name\":\"userIds\",\"description\":\"项目成员字符串\",\"remark\":\"例：1,2,3\"},{\"tid\":3,\"name\":\"updateDate\",\"description\":\"最后修改时间\",\"remark\":\"\"},{\"tid\":4,\"name\":\"updateDate\",\"description\":\"创建时间\",\"remark\":\"\"}]', 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('74', '20', '63', '查询项目接口列表', 'api/list', '查询项目接口列表', null, 'get', '[{\"tid\":0,\"name\":\"id\",\"type\":\"int\",\"isNeed\":true,\"description\":\"项目id\"}]', '[]', '[{\"tid\":0,\"name\":\"id\",\"description\":\"模块id\",\"remark\":\"\"},{\"tid\":1,\"name\":\"name\",\"description\":\"模块名称\",\"remark\":\"\"},{\"tid\":2,\"name\":\"projectId\",\"description\":\"所属项目id\",\"remark\":\"\"},{\"tid\":4,\"name\":\"pid\",\"description\":\"所属模块\",\"remark\":\"0:自己是模块\"}]', 'tianyu', null, null, null, null);
INSERT INTO `api` VALUES ('75', '20', '63', '删除接口', 'api/del', '删除接口', null, 'get', '[{\"tid\":0,\"name\":\"id\",\"type\":\"int\",\"isNeed\":true,\"description\":\"接口id\"}]', '[]', '[]', 'tianyu', null, null, null, null);

-- ----------------------------
-- Table structure for dynamic
-- ----------------------------
DROP TABLE IF EXISTS `dynamic`;
CREATE TABLE `dynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dynamic
-- ----------------------------

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO `position` VALUES ('1', 'java开发', null);
INSERT INTO `position` VALUES ('2', 'ios开发', null);
INSERT INTO `position` VALUES ('3', 'android开发', null);
INSERT INTO `position` VALUES ('4', 'html5开发', null);
INSERT INTO `position` VALUES ('6', 'php开发', null);
INSERT INTO `position` VALUES ('7', '测试', null);
INSERT INTO `position` VALUES ('8', '项目经理', null);

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '项目名',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_date` datetime DEFAULT NULL COMMENT '创建日期',
  `update_date` datetime DEFAULT NULL COMMENT '修改日期',
  `version` varchar(255) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('15', '发薪贷', '啊啊啊啊', '2017-12-24 14:33:32', '2017-12-26 09:54:00', null);
INSERT INTO `project` VALUES ('16', '急借通', '大大地', '2017-12-24 14:34:29', '2017-12-26 15:20:54', null);
INSERT INTO `project` VALUES ('17', '量子互助', '惹我惹我', '2017-12-24 14:34:55', '2017-12-26 14:08:18', null);
INSERT INTO `project` VALUES ('18', '合规', '广告歌', '2017-12-24 14:37:06', '2017-12-26 14:08:20', null);
INSERT INTO `project` VALUES ('19', '合规2.0', '广告歌', '2017-12-24 14:37:15', '2017-12-26 14:08:24', null);
INSERT INTO `project` VALUES ('20', 'superAPI', '接口管理系统接口管理系统', '2017-12-24 14:39:31', '2017-12-27 16:42:23', null);
INSERT INTO `project` VALUES ('25', 'test', '', '2017-12-27 17:18:19', '2017-12-27 17:18:19', null);
INSERT INTO `project` VALUES ('26', 'test2', null, '2017-12-27 17:18:22', '2017-12-27 17:18:22', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `position_id` int(11) DEFAULT NULL COMMENT '职位id',
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'tianyu', 'e10adc3949ba59abbe56e057f20f883e', '1', null);
INSERT INTO `user` VALUES ('5', 'tianyu1', 'e10adc3949ba59abbe56e057f20f883e', '1', '2017-12-22 14:06:38');
INSERT INTO `user` VALUES ('6', 'tianyu2', 'e10adc3949ba59abbe56e057f20f883e', '3', '2017-12-22 14:07:27');
INSERT INTO `user` VALUES ('7', 'admin', '0192023a7bbd73250516f069df18b500', '8', '2017-12-22 17:41:01');
INSERT INTO `user` VALUES ('8', '田雨', 'e10adc3949ba59abbe56e057f20f883e', '8', '2017-12-27 16:41:47');

-- ----------------------------
-- Table structure for user_project
-- ----------------------------
DROP TABLE IF EXISTS `user_project`;
CREATE TABLE `user_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `project_id` int(11) NOT NULL COMMENT '项目id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_project
-- ----------------------------
INSERT INTO `user_project` VALUES ('65', '1', '16');
INSERT INTO `user_project` VALUES ('66', '5', '16');
INSERT INTO `user_project` VALUES ('67', '6', '16');
INSERT INTO `user_project` VALUES ('68', '7', '16');
INSERT INTO `user_project` VALUES ('72', '6', '17');
INSERT INTO `user_project` VALUES ('73', '7', '17');
INSERT INTO `user_project` VALUES ('74', '1', '18');
INSERT INTO `user_project` VALUES ('75', '7', '18');
INSERT INTO `user_project` VALUES ('76', '7', '19');
INSERT INTO `user_project` VALUES ('79', '7', '21');
INSERT INTO `user_project` VALUES ('80', '7', '22');
INSERT INTO `user_project` VALUES ('81', '1', '15');
INSERT INTO `user_project` VALUES ('82', '7', '15');
INSERT INTO `user_project` VALUES ('83', '5', '15');
INSERT INTO `user_project` VALUES ('88', '7', '23');
INSERT INTO `user_project` VALUES ('89', '7', '24');
INSERT INTO `user_project` VALUES ('90', '1', '20');
INSERT INTO `user_project` VALUES ('91', '7', '20');
INSERT INTO `user_project` VALUES ('92', '8', '20');
INSERT INTO `user_project` VALUES ('93', '8', '25');
INSERT INTO `user_project` VALUES ('94', '8', '26');
