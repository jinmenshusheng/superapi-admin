package com.ty.superapi.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.xiaoleilu.hutool.util.CollectionUtil;

import java.io.Serializable;
import java.util.Set;

/**
 * 请求返回的最外层对象
 *
 * @author tianyu
 * @since 2017/11/15
 */
public class Result implements Serializable {
    //状态码
    private int code;
    //必要的提示信息
    private String msg;
    //业务数据
    private Object data;

    public Result(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 包装成功信息
     *
     * @param data 返回数据
     * @return json字符串
     */
    public static String success(Object data) {
        Result result = new Result(StatusCode._200.getCode(), StatusCode._200.getMsg(), data);
        return JSONObject.toJSONString(result, SerializerFeature.WriteNullStringAsEmpty);
    }

    /**
     * 包装成功信息
     *
     * @param data            返回数据
     * @param serializeFilter 序列化filter
     * @return json字符串
     */
    public static String success(Object data, SerializeFilter serializeFilter) {
        Result result = new Result(StatusCode._200.getCode(), StatusCode._200.getMsg(), data);
        return JSONObject.toJSONString(result, serializeFilter, SerializerFeature.WriteNullStringAsEmpty);
    }

    /**
     * 包装成功信息
     *
     * @return json字符串
     */
    public static String success() {
        Result result = new Result(StatusCode._200.getCode(), StatusCode._200.getMsg());
        return JSONObject.toJSONString(result);
    }

    /**
     * 包装错误信息
     *
     * @return json字符串
     */
    public static String error(StatusCode StatusCode) {
        Result result = new Result(StatusCode.getCode(), StatusCode.getMsg());
        return JSONObject.toJSONString(result);
    }

    /**
     * 包装错误信息
     *
     * @param statusCode 错误码
     * @param data       返回数据
     * @return json字符串
     */
    public static String error(StatusCode statusCode, Object data) {
        Result result = new Result(statusCode.getCode(), statusCode.getMsg(), data);
        return JSONObject.toJSONString(result, SerializerFeature.WriteNullStringAsEmpty);
    }

    /**
     * 设置JSON序列化包含字段
     *
     * @param c    序列化类
     * @param name 包含字段
     * @return SimplePropertyPreFilter
     */
    public static SimplePropertyPreFilter includes(Class<?> c, String... name) {
        SimplePropertyPreFilter filter = new SimplePropertyPreFilter(c, name);
        return filter;
    }

    /**
     * 设置JSON序列化排除字段
     *
     * @param name 排除字段
     * @return SimplePropertyPreFilter
     */
    public static SimplePropertyPreFilter excludes(String... name) {
        SimplePropertyPreFilter filter = new SimplePropertyPreFilter();
        Set<String> set = filter.getExcludes();
        set.addAll(CollectionUtil.newHashSet(name));
        return filter;
    }

    // get set
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}