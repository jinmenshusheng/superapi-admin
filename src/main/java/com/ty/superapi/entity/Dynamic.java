package com.ty.superapi.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tianyu
 * @since 2017-12-22
 */
public class Dynamic extends Model<Dynamic> {

    private static final long serialVersionUID = 1L;

	private Integer id;
	@TableField("user_id")
	private Integer userId;
	private String type;
	private String operation;
	@TableField("create_date")
	private Date createDate;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Dynamic{" +
			"id=" + id +
			", userId=" + userId +
			", type=" + type +
			", operation=" + operation +
			", createDate=" + createDate +
			"}";
	}
}
