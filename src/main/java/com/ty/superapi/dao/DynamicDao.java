package com.ty.superapi.dao;

import com.ty.superapi.entity.Dynamic;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author tianyu
 * @since 2017-12-22
 */
public interface DynamicDao extends BaseMapper<Dynamic> {

}