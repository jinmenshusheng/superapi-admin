package com.ty.superapi.dao;

import com.ty.superapi.entity.UserProject;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
public interface UserProjectDao extends BaseMapper<UserProject> {

}