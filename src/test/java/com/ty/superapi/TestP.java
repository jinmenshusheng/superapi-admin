package com.ty.superapi;

import com.xiaoleilu.hutool.crypto.SecureUtil;
import com.xiaoleilu.hutool.crypto.symmetric.SymmetricAlgorithm;
import com.xiaoleilu.hutool.crypto.symmetric.SymmetricCrypto;
import com.xiaoleilu.hutool.util.CharsetUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.junit.Assert;
import org.junit.Test;

public class TestP {

    @Test
    public void test() {
        String key = "tianyu1234567890";
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key.getBytes());
        String s = aes.encryptHex("1-tianyu");
        System.out.println(s);

        System.out.println(aes.decryptStr(s, CharsetUtil.CHARSET_UTF_8));
    }
}
